# GPU Usage Tutorial
This tutorial will demonstrate how to enable python deep learning frameworks
(e.g., Tensorflow or Pytorch) to use GPUs on the CCI AI Testbed.

## Using Base OS
This procedure will describe how to use GPUs without Docker.

### Pre-Requisites
1. **Install Anaconda:** To install anaconda, follow the tutorial located in this repository at `$PROJECT_DIR/install_anaconda`.

### Procedure
1. Refer to the CCI AI Assurance Testbed wiki for login information.

2. Clone this repository to an easy to find/use location by running:
  ```
  # Cloning the repository
  git clone https://code.vt.edu/cci-ai-testbed/documentation.git

  # Setting the project directory environment variable for later use
  cd documentation
  export PROJECT_DIR=$(pwd)
  ```

3. Create a new anaconda environment and install tensorflow by running the following command:
```
cd $PROJECT_DIR
conda env create -f host_access/gpu_usage/environment.yml
```
4. Activate your anaconda environment:
```
conda activate ml_dev_env
```
5. Run a tensorflow test command using gpus:
```
python -c "import tensorflow as tf; print(tf.reduce_sum(tf.random.normal([1000, 1000])))"
```

## Using Docker
This procedure will describe how to use GPUs from within a container (e.g., Docker container).

### Pre-Requisites
1. **Install Docker-Rootless:** Please follow the directions located [here](https://code.vt.edu/cci-ai-testbed/docker-rootless).

### Procedure
1. Pull and run an example container using gpus:
```
docker run --gpus=all -it --rm tensorflow/tensorflow "python -c 'import tensorflow as tf; print(tf.reduce_sum(tf.random.normal([1000, 1000])))'"
```
