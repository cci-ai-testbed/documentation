# Storage
## Table of Contents
- [Overview](#overview)
- [Usage](#usage)
- [Tutorials](#tutorials)
- [Key Terms](#key-terms)

## Overview
The CCI AI Testbed provides `object`-based storage to its users.  `Object storage` is ubiquitous across cloud computing and Platform-as-a-Service (PaaS) providers.  

`Object storage` provides the following features:
1. Externally Accessible​
1. Authenticated​
1. Permissions/Policies per bucket​
1. Logging​
1. Diverse Set of Clients:
  1. Python SDK​
  1. Java SDK​
  1. Command Line Utilities (CLI)

An `object` is a collection of data, a globally unique identifier, and metadata.  An example of an `object` may be an image, text, video, etc.  A collection of `objects` are stored in `buckets`.

## Usage
End-user interactions with the `object store` (e.g., create/view `buckets` or upload `objects`) are conducted via the `S3` application programming interface (API).  The `S3 API` was originally developed by Amazon Web Services and has since become an industry standard for interacting with `object stores`.  Note: there will be tools (e.g., `aws cli`) we utilize that bare Amazon's name; however, this is tools is still usable for the interactions with the CCI AI Testbed.

The `S3 API` can be accessed by a diverse set of clients.  Please refer to the [tutorials section](tutorials) for specific instructions on how to use your specific client.

## Tutorials
Please refer to the [tutorials](tutorials) README.md for specific instructions on how to use your specific client with the `object store`

## Access Control
For a more comprehensive and general overview of authentication and authorization, please review the documentation located [here](../authentication_authorization/README.md).

`Object storage` access control is managed via role-based access control (RBAC).  `Buckets` are `namespaced`/`project` `resources` within `kubernetes`.  As such, those who can view `resources` within the `project` may view the `access credentials` for each `bucket` within the `namespace`/`project` and therefore gain access to said `bucket`.  For more information on who can view `resources` within your project, see the documentation located [here](../authentication_authorization/README.md).

For example, `UserA` is administrator for `Project1`.  `UserB` is a viewer of `Project1`.  `UserC` is not an admin or viewer of `Project1`.  Both `UserA` and `UserB` can access a `bucket` created in `Project1`, but `UserC` cannot.

## Key Terms
- `object storage`:
- `bucket`:
- `object`:
- `aws`:  a command-line utility (CLI) developed by Amazon Web Services (AWS) that provides several functionalities useful for interacting with the CCI AI Object Storage (e.g., storing `access credentials`).
- `AWS_ACCESS_KEY_ID`:
- `AWS_SECRET_ACCESS_KEY`:
- `access credentials`: the pair of `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`
- `SDK`:  software development kit.
- `Portal`:
- `Project`:
- `Namespace`:
