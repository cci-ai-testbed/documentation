# Logging Into The CCI AI Testbed

The CCI AI Testbed is an advanced computing platform designed for AI and ML workloads to help researchers and others preform cutting edge compute tasks with out the hassle of all the infrastructure management.

Security is a critical part to the management of the CCI AI Testbed. Industry best practices like encryption, 2 factor authentication, security groups and roles, quotas, and other measures are leaveraged to maintain the platforms integrity.

We understand that with the increased level of security, we often trade-off a certain level of simplicty or user-experince. We are trying to balances these concepts, despite them often being at complete odds some times, so please understand we are evolving our practices and documenation as we go.

This is intended to be a living document and will elvolve with us, thank you.  

## First Time Login

As a new user to the platform, there are two tasks we must complete for a first time login to the CCI AI Testbed.

1. Setup a 2 Factor Authentication (2fa) client that supports TOTP
2. Reset the original password

Once we have completed these tasks, subsequent logins can proceed normally with just the user's password and pin from the 2fa client.

### Setup 2 Factor Authentication (2fa)

2fa has become a standard tool in modern security for preventing brute force attacks and stolen credentials. It relies on the concept of something you know (your password) and something you have (e.g. your phone, with an app). You may already have an application installed on an iOS or Android device suitable for handling the role of the 2fa client.

Most 2fa clients with TOTP support should work. We have tested several and would reccomend checking the list to see if you already have one of them installed. If not, we reccomend choosing a client from the list, as they have all been confirmed to work. Other clients may work as well, but are currently not tested.

Once a client has been setup, you will need to stick with that client. If there was an issue during setup, or you need to switch clients, please contact <insert support contanct info> to have your credentials reset.

#### Tested 2fa Clients

These clients are in no particular order. If you already have one of these clients installed, feel free to use it for your CCI AI Testbed account if you wish to avoid installing an additional app. However if you do not have a client listed or your chosen client is not compatible, please install **one** of the apps listed below.

##### iOS

* Duo
* Google Authenticator
* Authenticator
* FreeOTP
* 2FA Authenticator
* Authy
* Microsoft Authenticator

##### Android

* AGIS Authenticator
* AndOTP
* Duo
* Google Authenticator
* FreeOTP
* 2FA Authenticator
* Microsoft Authenticator

#### SSL Certs

Currently the SSL certs are self signed. This will appear as a warning in the browser letting the user know the address can not be verified. This is expected behavior currently and will be adressed in a future release.

Navigate to

https://rancher.apps.ncr1.aitb.cyberinitiative.org

Upon first load, you will be asked to accept the certificate.

#### Login Using Provided Credentials

You will need to click on the "Log on with Keycloak" button. This will redirect you for authentication, through Keycloak 2FA.

![alt text](rancher_keycloak_login.png "Rancher Keycloak Login")

You should have your username and initial password to login to the CCI AI Testbed.

![alt text](login_prompt.png "Login Box")

Usernames by default will follow the convention of first letter of the first name, concatonated with the last name.

Passwords will be sent to you and will only work within a short window of when they are sent. During your first login you will be require to reset your password.

![alt text](login_creds.png "Login Box Filled In")

#### Register 2fa Client

After we have our 2fa client app installed, we need to register it with the CCI AI Testbed to be associated with our login account.

We will not be covering specific clients, how to install them, or how to add a new key. Please see the documentation for your chosen client on how to complete these steps.

Once you are ready to register a new 2fa key, continue with these instructions.

##### Scan QR Code

The easiest way to add a new 2fa key is to use the camera built into your mobile device and scan the QR code that is presented after you provide your initial login credentials.

Once you scan the QR code, the app should receive the correct configuration and let you save the new 2fa key. The app should provide you with a new 6 digit key, every 30 seconds.

![alt text](totp_setup.png "QR Scan")

##### Manual Entry

If you are unable to scan the QR code, you may need to use the manual entry option to add a new 2fa key.

Click the link on the screen that says `Unable to scan?` to load the instructions and manual key value.

Entry the key provided on screen in `Step 2` into 2fa mobile app and contiue with seup.

![alt text](manual_entry.png "Manual Entry")

##### Finish 2fa Configuration

Enter the most recent key (6 digits only) you see on screen into the box called `One-time code`

Name your device by giving it a lable in the box called `Device Name`

Click `Submit`

![alt text](totp_entry.png "TOTP Values")

### Reset Password

After successfully setting up your 2fa client, you will be prompted to reset your password.

Please pick a new one now.

`NOTE: It is highly reccomended you use a password manager. Have it both generate a random password and store it for you`

![alt text](password_reset.png "Password Reset Prompt")

## Subsequent Logins

Once you have setup your 2fa client, and reset your initial password, you will be able to preform subsequent logins with just your username, password, and one time code provided by your app (which changes every 30 secods).

After providing your username and password to the inital login screen, you will be prompted for the 6 digit 2fa key provided by the app you previously configured.

![alt text](2fa_login.png "2fa Key Request")

If you provided the correct login credentials and 2fa key, the dashboard will load.
