# Development Environments
## Table of Contents
- [Quickstart](#quickstart)
- [Overview](#overview)
- [Usage](#usage)
- [Tutorials](#tutorials)
- [Freqently Asked Questions](#frequently-asked-questions)
- [Key Terms](#key-terms)

## Quickstart
1. In your web browser, navigate to https://jupyterhub.apps.ncr1.aitb.cyberinitiative.org.
2. Authenticate using Keycloak 2FA. (Also desribed here [Login](login))
3. Choose your desired environment and click "Start" (e.g., `Datascience environment`, `GPU powered environment`)
4. Start your research!


## Overview
The CCI AI Testbed provides users with access to interactive python development environments using `jupyter notebooks`.  The `jupyter notebook` web browser-based environment is made available without any configuration required by the end user (e.g., operating system installation, security configuration, package maintenance, etc.).  

Get up and running in three simple steps:
1. After logging in as described above, choose your desired environment and click "Start" (e.g., `Datascience environment`)

![environment selection](jupyter_selection.png)

2. Using the Datascience environment notebook, as an example, create a new `notebook`

![new notebook](new_notebook.png)

3. You may also Upload and utilize your own notebooks

![jupyter lab](jupyter_lab.png)

## Usage
A user interfaces with `jupyter notebooks` through their web browser.  The `jupyter notebook` runs inside a `container` on the testbed.  The hardware resources dedicated to the `container` (and consequently, the `jupyter notebook`) are specified by the user prior to starting the notebook.

When the user's `container` starts, a `persistent volume` is dynamically provisioned and mounted to the container.  When the users shuts down their `jupyter notebook`, this `persistent volume` will remain persistent.  When the user starts their notebook again, the `persistent volume` containing their files/data will be remounted to the new `container`.

## Additional Information
- [I want to utilize a GPU while running code in my jupyter notebook.](tutorials/using_gpus)

## Frequently Asked Questions
1. **Will my files persist after I shutdown my `jupyter notebook`?** \
Yes, when your `container` starts, a `persistent volume` is dynamically provisioned and mounted to the container.  When you shutdown your `jupyter notebook`, this `persistent volume` will remain persistent.  Simply start your `jupyter notebook` again and this `persistent volume` will be re-mounted to your container for easy access.
1. **I don't have enough space in my `container` to store my dataset.** \
The `persistent volume` mounted to the `container` provides users with 50GB of persistent storage for files (e.g., code, small datasets). If you require additional storage, please send a request to support@cci-ai-testbed.atlassian.net, including your requested storage size and need, along with any other details that might helps us assist you.

## Key Terms
- `container`:
- `jupyter notebook`:
- `persistent volume`:
- `notebook image`:
- `S3 object storage`:
